package com.proyecto.personal.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.proyecto.personal.entidades.Doctor;
import com.proyecto.personal.entidades.Especialidad;
import com.proyecto.personal.repositorios.IDoctorRepository;
import com.proyecto.personal.repositorios.IEspecialidadRepository;

@Service
public class DoctorService {
	@Autowired
	IDoctorRepository rdoctor;
	@Autowired
	IEspecialidadRepository respecialidad;
	
	public List<Doctor> getAll(){
		return (List<Doctor>) rdoctor.findAll();
		
	}
	
	public Boolean saveOrUpdate(Doctor entity) {
		try {
			rdoctor.save(entity);
			return true;
					
		}catch(Exception e) {
			return false;
		}

	}

	public Boolean delete(Doctor entity) {
		try {
			rdoctor.delete(entity);
			return true;
					
		}catch(Exception e) {
			return false;
		}

	}

	public Especialidad getEspecialidad(Integer id) {
		return respecialidad.findById(id).get();
	}
	public Doctor getDoctor(Integer id) {
		return rdoctor.findById(id).get();
	}
		
	

}
