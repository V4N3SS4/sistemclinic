package com.proyecto.personal.controladores;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import com.proyecto.personal.entidades.Doctor;
import com.proyecto.personal.repositorios.IDoctorRepository;
import com.proyecto.personal.service.DoctorService;

@Controller
@RequestMapping("doctores")
public class DoctorController {

	//Repositorio para el manejo de datos
	@Autowired
	DoctorService doctorService;
	
	@GetMapping(value="index")
    public String index() {
        return new String("/views/doctor/doctor");
    }
	 
	 @GetMapping(value="all",produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseBody
	    public List<Doctor> getAllDoctor() {
	        return (List<Doctor>) doctorService.getAll();
	    }
	 @GetMapping(value="save")
	 @ResponseBody																					//asi se manda a llamar el id de especialidad	
	    public HashMap<String, String> save(@RequestParam String nombre, @RequestParam String direccion, @RequestParam Integer idEspecialidad) {
		 Doctor doctor=new Doctor();//creando objeto de doctor
		 
		 HashMap<String, String> jsonReturn=new HashMap<>();
		 //asignado datos al objeto de doctor
		 doctor.setNombre(nombre);
		 doctor.setDireccion(direccion);
		 doctor.setEspecialidad(doctorService.getEspecialidad(idEspecialidad));
		 //manejando cualquier excepcion de error
		 try {
			 doctorService.saveOrUpdate(doctor);//guardando registro de doctor
			 jsonReturn.put("estado", "OK");
			 jsonReturn.put("mensaje", "Registro guardado");
			 
			 return jsonReturn;
		 }catch(Exception e) {
			 jsonReturn.put("estado", "ERROR");
			 jsonReturn.put("mensaje", "Registro no guardado"+e.getMessage());
			 return jsonReturn;
		 }
		 
	    }
	 //Eliminar
	 @GetMapping(value="delete/{id}")
	 @ResponseBody
	    public HashMap<String,String> delete (@PathVariable Integer id) {
		 
	        HashMap<String, String> jsonReturn=new HashMap<>();
	        
	        try {
	        	//buscando registro
	        	Doctor doctor=doctorService.getDoctor(id);
	        	//eliminando registro
	        	doctorService.delete(doctor);
				 jsonReturn.put("estado", "OK");
				 jsonReturn.put("mensaje", "Registro eliminado");
				 return jsonReturn;
	        	
	        }catch(Exception e) {
				 jsonReturn.put("estado", "ERROR");
				 jsonReturn.put("mensaje", "Registro no guardado"+e.getMessage());
				 return jsonReturn;
			 }
	    }
	 
	 //actualizar
	 @GetMapping(value="update/{id}")
	 @ResponseBody
	    public HashMap<String, String> update(@RequestParam Integer id, @RequestParam String nombre, @RequestParam String direccion, @RequestParam Integer idEspecialidad) {
		 Doctor doctor=new Doctor();//creando objeto de doctor		 
		 HashMap<String, String> jsonReturn=new HashMap<>();
		 
		 //asignado datos al objeto de doctor
		 doctor.setId(id);
		 doctor.setNombre(nombre);
		 doctor.setDireccion(direccion);
		 doctor.setEspecialidad(doctorService.getEspecialidad(idEspecialidad));
		 
		 //manejando cualquier excepcion de error
		 try {
			 doctorService.saveOrUpdate(doctor);//guardando registro de doctor
			 jsonReturn.put("estado", "OK");
			 jsonReturn.put("mensaje", "Registro actualizado");			 
			 return jsonReturn;
			 
		 }catch(Exception e) {
			 jsonReturn.put("estado", "ERROR");
			 jsonReturn.put("mensaje", "Registro no actualizado"+e.getMessage());
			 return jsonReturn;
		 }
		 
	    }
	 
}
